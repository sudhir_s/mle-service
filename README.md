## Start S3 Server, for mlflow artifact, Prefect workflow checkpoint

MinIO is an Amazon S3 compatible server-side software storage stack.

1. docker run -p 9000:9000 --name minio_mlflow \
-e MINIO_ACCESS_KEY=<s3 access key> \
-e MINIO_SECRET_KEY=<s3 secret key> \
-v <data mouting path>:/data \
-v <minio config mouting path>:/root/.minio \
minio/minio server /data
2. Ex: docker run -p 9000:9000 --name minio_mlflow \
-e MINIO_ACCESS_KEY=heal123 \
-e MINIO_SECRET_KEY=heal@123 \
-v /home/sudhirshetty/heal_ml/mlflow_artifact/data:/data \
-v /home/sudhirshetty/heal_ml/mlflow_artifact/config:/root/.minio \
minio/minio server /data
3. http://127.0.0.1:9000/minio/
4. create bucket 'mlflow' and 'mldata'
5. venv mle_tech
6. firewall-cmd --add-port=41304/tcp --zone=public --permanent && firewall-cmd --reload
7. sudo ./minio server /minio --console-address ":41304"

## My sql for mlflow back end store

1. docker run --name=<container name> -d mysql/mysql-server
2. docker logs mysql_mlflow 2>&1 | grep GENERATED
[Entrypoint] GENERATED ROOT PASSWORD: Z&1w?qBds04DK8I+1&yY^kDgSi6_=69#
3. sudo docker exec -it [container_name] bash
4. mysql -uroot -p
5. ALTER USER 'root'@'localhost' IDENTIFIED BY '[newpassword]';
8. ALTER USER 'root'@'localhost' IDENTIFIED BY '<root password>';
9. CREATE USER '<new user name>' IDENTIFIED BY '<password for new user>';
10. CREATE DATABASE mlflowruns;
11. GRANT ALL PRIVILEGES ON mlflowruns.* TO '<new user name>';

## To run MLFlow UI

1. Open terminal, run below comand for set env variables
export MLFLOW_S3_ENDPOINT_URL=http://<s3 host>:<port>
export AWS_ACCESS_KEY_ID=<s3 access ey>
export AWS_SECRET_ACCESS_KEY=<s3 secret key>
2. mlflow server -p <mlflow server port> --host 0.0.0.0 --backend-store-uri 
   mysql+pymysql://'<new mysql user name>':'<new mysql password>'@<mysql ip>:3306/mlflowruns 
   --default-artifact-root s3://mlflow
3. ex: mlflow server -p 9001 --host 0.0.0.0 --backend-store-uri mysql+pymysql://mluser:ml123@172.17.0.2:3306/mlflowruns --default-artifact-root s3://mlobjectstore
   
## To run Dask cluster
1. Open terminal, run below comand for set env variables
export MLFLOW_S3_ENDPOINT_URL=http://<s3 host>:<port>
export AWS_ACCESS_KEY_ID=<s3 access ey>
export AWS_SECRET_ACCESS_KEY=<s3 secret key>
export MLFLOW_TRACKING_URI=http://<mlflow server ip>:<mlflow server port>